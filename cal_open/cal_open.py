# Import the necessary modules
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout

# Create a subclass of App to build our calculator app
class CalculatorApp(App):
    # This method is called when the app is first run
    def build(self):
        # Create a grid layout to hold the calculator buttons
        layout = GridLayout(cols=4, spacing=3, padding=3)

        # Create the buttons for the numbers and operations
        btn_7 = Button(text='7')
        btn_8 = Button(text='8')
        btn_9 = Button(text='9')
        btn_plus = Button(text='+')
        btn_4 = Button(text='4')
        btn_5 = Button(text='5')
        btn_6 = Button(text='6')
        btn_minus = Button(text='-')
        btn_1 = Button(text='1')
        btn_2 = Button(text='2')
        btn_3 = Button(text='3')
        btn_multiply = Button(text='*')
        btn_0 = Button(text='0')
        btn_dot = Button(text='.')
        btn_equal = Button(text='=')
        btn_divide = Button(text='/')

        # Add the buttons to the layout
        layout.add_widget(btn_7)
        layout.add_widget(btn_8)
        layout.add_widget(btn_9)
        layout.add_widget(btn_plus)
        layout.add_widget(btn_4)
        layout.add_widget(btn_5)
        layout.add_widget(btn_6)
        layout.add_widget(btn_minus)
        layout.add_widget(btn_1)
        layout.add_widget(btn_2)
        layout.add_widget(btn_3)
        layout.add_widget(btn_multiply)
        layout.add_widget(btn_0)
        layout.add_widget(btn_dot)
        layout.add_widget(btn_equal)
        layout.add_widget(btn_divide)

        # Create a label to display the result of the calculation
        self.result_label = Label(text='')
        layout.add_widget(self.result_label)

        # Return the layout to be displayed in the app
        return layout

# Run the app
if __name__ == '_main_':
    CalculatorApp().run()