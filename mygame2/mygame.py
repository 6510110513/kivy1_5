from kivy.app import App
from kivy.uix.widget import Widget
from kivy.graphics import Rectangle
from kivy.core.window import Window
from kivy.clock import Clock
from kivy.core.audio import SoundLoader
import os
#os.environ["KIVY_AUDIO"] = "ffpyplayer"
class GameWidget(Widget):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        with self.canvas:
            self.hero = Rectangle(source='hero.png', pos=(200, 300), size=(100,100))
class MyApp(App):
    def build(self):
        return GameWidget()
if __name__ == '__main__':
 app = MyApp()
 app.run()